
//
//  LXRFriendTrendsViewController.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/6.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LXRFriendTrendsViewController.h"
#import "LXRRecommendViewController.h"
#import "LXRLoginRegisterViewController.h"


@interface LXRFriendTrendsViewController ()

@end

@implementation LXRFriendTrendsViewController

/**加载视图*/
-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self = [[UIStoryboard storyboardWithName:@"LXRFriendTrendsViewController" bundle:nil]instantiateViewControllerWithIdentifier:@"LXRFriendTrendsViewController"];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    //设置背景色
    self.view.backgroundColor = LXR_BJ_Color;

    //初始化设置Item
    [self setupNavItem];
    
}
/**初始化设置Item*/
-(void)setupNavItem{
    self.navigationItem.title = @"我的关注";
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithImage:@"friendsRecommentIcon" highImage:@"friendsRecommentIcon-click" target:self action:@selector(FriendClick)];
}
/**实现Item点击事件---->推荐关注*/
-(void)FriendClick{
   // LXRLogFunc;
    LXRRecommendViewController* recommend = [[LXRRecommendViewController alloc]init];
    [self.navigationController pushViewController:recommend animated:YES];
}

#pragma mark - 点击注册按钮
- (IBAction)LoginBtn {
    LXRLoginRegisterViewController* login = [[LXRLoginRegisterViewController alloc]init];
    
    
    
    [self presentViewController:login animated:YES completion:nil];
    
}

@end
