//
//  LXRRecommecdLeftCell.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/8.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LXRRecommecdLeftCell.h"
#import "LXRRecommendLeftModel.h"

@interface LXRRecommecdLeftCell ()
/**选中时显示的指示器*/
@property (weak, nonatomic) IBOutlet UIView *SelctedView;


@end

@implementation LXRRecommecdLeftCell

- (void)awakeFromNib {
    //设置cell背景色
    self.backgroundColor = LXR_RGB_Color(244, 244, 244);
    
    
}

-(void)setModel:(LXRRecommendLeftModel *)model{
    _model = model;
    self.textLabel.text = model.name;
}


-(void)layoutSubviews{
    [super layoutSubviews];
    
    //重新调整内部textLable的Frame,防止view被lable覆盖看不到
    self.textLabel.y = 2;
    self.textLabel.height = self.contentView.height - 2 * self.textLabel.y;
    
}
#pragma mark - 重写选中方法,系统默认选中将所有子控件显示为高亮状态---------重点

/*可以在这个方法中监听cell的选中和取消选中*/
-(void)setSelected:(BOOL)selected animated:(BOOL)animated{
    //selected参数: 会打印出选中第几组第几行信息
    [super setSelected:selected animated:animated];

    self.SelctedView.hidden = !selected;
    
    //设置文字颜色,如果是选中状态是红色,如果不是就是正常颜色
    self.textLabel.textColor = selected ? LXR_RGB_Color(219, 21, 26) : LXR_RGB_Color(78, 78,78);
    
    //设置正常状态下文本颜色
    //self.textLabel.textColor = LXR_RGB_Color(78, 78, 78);
    //默认选中cell时textLable就会变成高亮颜色
    //self.textLabel.highlightedTextColor = LXR_RGB_Color(219, 21, 26);
    
}
@end
