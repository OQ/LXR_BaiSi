//
//  LXRRecommendRightCell.h
//  01- 百思不得姐
//
//  Created by mac on 16/6/10.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LXRRecommendRightModel;
@interface LXRRecommendRightCell : UITableViewCell

/**模型*/
@property(nonatomic,strong)LXRRecommendRightModel* model;
@end
