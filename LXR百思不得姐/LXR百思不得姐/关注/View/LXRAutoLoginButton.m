//
//  LXRAutoLoginButton.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/13.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LXRAutoLoginButton.h"

@implementation LXRAutoLoginButton

-(void)setup{
    //文字居中
    self.titleLabel.textAlignment = NSTextAlignmentCenter;

}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setup];
    }
    return self;
}



-(void)awakeFromNib{
    [self setup];
}




-(void)layoutSubviews{

    [super layoutSubviews];
    
    //调整图片
    self.imageView.x = 0;
    self.imageView.y = 0;
    self.imageView.width = self.width;
    self.imageView.height = self.imageView.width;
    
    //调整文字
    self.titleLabel.x = 0;
    self.titleLabel.y = self.imageView.height;
    self.titleLabel.width = self.imageView.width;
    self.titleLabel.height = self.height - self.titleLabel.y;
    
}

@end
