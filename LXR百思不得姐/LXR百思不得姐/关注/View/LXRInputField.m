//
//  LXRInputField.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/15.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LXRInputField.h"
#import <objc/message.h>

static NSString* const LXRplaceholderColor = @"_placeholderLabel.textColor";

@implementation LXRInputField

+(void)initialize{

    unsigned int count = 0;
    //拷贝出所有成员变量列表
    Ivar* ivars = class_copyIvarList([UITextField class], &count);
    
    for (int i =0; i<count; i++) {
        //取出成员变量
        Ivar ivar = *(ivars + i);
        //Ivar ivar = ivars[i];
        //打印成员变量名字
        LXRLog(@"%s",ivar_getName(ivar));
    }
    //释放
    free(ivars);
}

-(void)awakeFromNib{
    
    //设置光标颜色和文字颜色一致
    self.tintColor = self.textColor;
    
    //开始不成为第一响应者
    [self resignFirstResponder];
}
/**
 当前文本框成为焦点是就会调用
 */
-(BOOL)becomeFirstResponder{
    //设置成为第一响应者占位文字颜色
    [self setValue:self.textColor forKeyPath:LXRplaceholderColor];
    return [super becomeFirstResponder];
}
/**
 当前文本框失去焦点是就会调用
 */
-(BOOL)resignFirstResponder{
    //设置放弃第一响应者占位文字颜色
    [self setValue:[UIColor grayColor] forKeyPath:LXRplaceholderColor];
    return [super resignFirstResponder];
}
@end
