//
//  LXREssenceViewController.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/6.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LXREssenceViewController.h"
#import "LXRTagsController.h"
#import "LXRTopicController.h"
#import "LXRHeadWindow.h"

@interface LXREssenceViewController ()<UIScrollViewDelegate>
/**文字底部指示器*/
@property(nonatomic,weak)UIView* bottomView;
/**当前选中临时按钮*/
@property(nonatomic,weak)UIButton* index;
/**标题栏整体View*/
@property(nonatomic,weak)UIView* titleView;
/** 底部的所有内容 */
@property (nonatomic, weak) UIScrollView *contentView;
@end

@implementation LXREssenceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    /****************添加一个Window,不管界面滚到什么位置,实现屏幕滚到最顶部功能*****************/
    [LXRHeadWindow show];
    
    //设置导航航
    [self setupNav];
    
    //初始化自控制器
    [self setupChildVc];
    
    //设置标题栏
    [self setupTitleView];
    
    //底部的ScrollView
    [self setupContentView];
    

    
}
/**初始化自控制器*/
-(void)setupChildVc{
   
    
    LXRTopicController* all = [[LXRTopicController alloc]init];
    all.title = @"全部";
    all.type = LXRTopicTypeAll;
    [self addChildViewController:all];
    
    LXRTopicController* video = [[LXRTopicController alloc]init];
    video.title = @"视频";
    video.type = LXRTopicTypeVideo;
    [self addChildViewController:video];
    
    LXRTopicController* voice = [[LXRTopicController alloc]init];
    voice.title = @"声音";
    voice.type = LXRTopicTypeVoice;
    [self addChildViewController:voice];
    
    LXRTopicController* picture = [[LXRTopicController alloc]init];
    picture.title = @"图片";
    picture.type = LXRTopicTypePicture;
    [self addChildViewController:picture];
    
    LXRTopicController* word = [[LXRTopicController alloc]init];
    word.title = @"段子";
    word.type = LXRTopicTypeWord;
    [self addChildViewController:word];
    
    
}
/**设置标题栏*/
-(void)setupTitleView{
    //设置标题栏整体View
    UIView* titleView = [[UIView alloc]init];
    titleView.x = 0;
    titleView.y = LXRTitlesViewY;
    titleView.width = self.view.width;
    titleView.height = LXRTitlesViewH;
    titleView.backgroundColor = [[UIColor whiteColor]colorWithAlphaComponent:0.7];
    [self.view addSubview:titleView];
    self.titleView = titleView;
    
    //添加文字底部指示器
    UIView* bottomView = [[UIView alloc]init];
    bottomView.height = 2;
    bottomView.tag = -1;
    bottomView.y = titleView.height - bottomView.height;
    bottomView.backgroundColor = [UIColor redColor];
    
    self.bottomView = bottomView;
    
    //添加标题按钮
    //标题文字
    CGFloat width = titleView.width / self.childViewControllers.count;
    CGFloat height = titleView.height;
    for (NSInteger i = 0; i<self.childViewControllers.count; i++) {
        UIButton *button = [[UIButton alloc] init];
        //设置标题tag 为滚动记录
        button.tag = i;
        button.height = height;
        button.width = width;
        button.x = i * width;
        UIViewController *vc = self.childViewControllers[i];
        //设置标题
        [button setTitle:vc.title forState:UIControlStateNormal];
        //        [button layoutIfNeeded]; // 强制布局(强制更新子控件的frame)
        //设置文字颜色
        [button setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor redColor] forState:UIControlStateDisabled];
        //设置字体大小
        button.titleLabel.font = [UIFont systemFontOfSize:14];
        [button addTarget:self action:@selector(titleClick:) forControlEvents:UIControlEventTouchUpInside];
        [titleView addSubview:button];

        //默认点击了第一个按钮
        if (i==0) {
            button.enabled = NO;
            self.index = button;
            
            //让按钮内部的lable根据文字内容来计算尺寸
            [button.titleLabel sizeToFit];
            //self.bottomView.Width = [titles[i] sizeWithAttributes:@{NSFontAttributeName:button.titleLabel.font}].width;
            self.bottomView.width = button.titleLabel.width;
            self.bottomView.centerX = button.centerX;
        }
    }
    
    [titleView addSubview:bottomView];
}
/**设置标题栏*/
-(void)titleClick:(UIButton*)button{
    
    //修改按钮状态
    self.index.enabled = YES;
    button.enabled = NO;
    self.index = button;
    
    //动画移动指示器
    [UIView animateWithDuration:0.25 animations:^{
        self.bottomView.width = button.titleLabel.width;
        self.bottomView.centerX = button.centerX;
    }];
    //滚动
    CGPoint offset = self.contentView.contentOffset;
    offset.x = button.tag * self.contentView.width;
    [self.contentView setContentOffset:offset animated:YES];
    
}
/**底部的ScrollView*/
-(void)setupContentView{
    //不要自动调整inset
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    UIScrollView* ContentView = [[UIScrollView alloc]init];
    ContentView.frame = self.view.bounds;
    ContentView.delegate = self;
    ContentView.contentSize = CGSizeMake(ContentView.width * self.childViewControllers.count, 0);
    ContentView.pagingEnabled = YES;
    [self.view insertSubview:ContentView atIndex:0];
    self.contentView = ContentView;
    // 添加第一个控制器的view
    [self scrollViewDidEndScrollingAnimation:ContentView];
    
}
/**设置导航航*/
-(void)setupNav{
    //设置导航栏标题
    self.navigationItem.titleView  = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"MainTitle"]];
    //添加左item
    self.navigationItem.leftBarButtonItem  =[UIBarButtonItem itemWithImage:@"MainTagSubIcon" highImage:@"MainTagSubIconClick" target:self action:@selector(ClickBtn)];
    //设置背景色
    self.view.backgroundColor = LXR_BJ_Color;
}
-(void)ClickBtn{
    LXRLogFunc;
    [LXRHeadWindow hide];
    LXRTagsController* tags = [[LXRTagsController alloc]init];
    [self.navigationController pushViewController:tags animated:YES];
}

#pragma mark - <UIScrollViewDelegate>代理方法
-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    //当前索引
    NSInteger index = scrollView.contentOffset.x / scrollView.width;
    // 取出子控制器
    UIViewController *vc = self.childViewControllers[index];
    vc.view.x = scrollView.contentOffset.x;
    vc.view.y = 0; // 设置控制器view的y值为0(默认是20)
    vc.view.height = scrollView.height; // 设置控制器view的height值为整个屏幕的高度(默认是比屏幕高度少个20)
    [scrollView addSubview:vc.view];

}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    [self scrollViewDidEndScrollingAnimation:scrollView];
    //点击按钮
    NSInteger index = scrollView.contentOffset.x / scrollView.width;
    [self titleClick:self.titleView.subviews[index]];
}
@end
