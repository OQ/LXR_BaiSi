//
//  LXRSquare.h
//  01- 百思不得姐
//
//  Created by mac on 16/6/30.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LXRSquareModel;
@interface LXRSquareButton : UIButton
/** 方块模型 */
@property(nonatomic,strong)LXRSquareModel* square;
@end
