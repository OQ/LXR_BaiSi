//
//  LXRMeViewController.m
//  01-百思不得姐
//
//  Created by xiaomage on 15/7/22.
//  Copyright (c) 2015年 小码哥. All rights reserved.
//

#import "LXRMeViewController.h"
#import "LXRMeCell.h"
#import "LXRMeFooterView.h"
#import "LXRSettingViewController.h"


@interface LXRMeViewController ()

@end

@implementation LXRMeViewController

static NSString *LXRMeId = @"me";

- (void)viewDidLoad
{
    [super viewDidLoad];
    /**设置导航条属性*/
    [self setupNav];
    /**初始化TableView*/
    [self setupTableView];
    

}
/**初始化TableView*/
- (void)setupTableView
{
    // 设置背景色
    self.tableView.backgroundColor = LXR_BJ_Color;
    // 分割线
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //注册
    [self.tableView registerClass:[LXRMeCell class] forCellReuseIdentifier:LXRMeId];
    
    // 调整header和footer
    self.tableView.sectionHeaderHeight = 0;
    self.tableView.sectionFooterHeight = LXRTopicCellMargin;
    
    // 调整inset
    self.tableView.contentInset = UIEdgeInsetsMake(LXRTopicCellMargin-35 , 0, 0, 0);
    // 设置footerView
    self.tableView.tableFooterView = [[LXRMeFooterView alloc]init];

}
#pragma mark - 设置导航条属性
/**设置导航条属性*/
- (void)setupNav
{
    // 设置导航栏标题
    self.navigationItem.title = @"我的";
    
    // 设置导航栏右边的按钮
    UIBarButtonItem *settingItem = [UIBarButtonItem itemWithImage:@"mine-setting-icon" highImage:@"mine-setting-icon-click" target:self action:@selector(settingClick)];
    UIBarButtonItem *moonItem = [UIBarButtonItem itemWithImage:@"mine-moon-icon" highImage:@"mine-moon-icon-click" target:self action:@selector(moonClick)];
    self.navigationItem.rightBarButtonItems = @[settingItem, moonItem];
}
/**点击设置*/
- (void)settingClick
{
    [self.navigationController pushViewController:[[LXRSettingViewController alloc]initWithStyle:UITableViewStyleGrouped] animated:YES];
}
/**点击月亮*/
- (void)moonClick
{
    LXRLogFunc;
}

#pragma mark - 数据源方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LXRMeCell *cell = [tableView dequeueReusableCellWithIdentifier:LXRMeId];
    
    if (indexPath.section == 0) {
        cell.imageView.image = [UIImage imageNamed:@"mine_icon_nearby"];
        cell.textLabel.text = @"登录/注册";
    } else if (indexPath.section == 1) {
        cell.textLabel.text = @"离线下载";
    }
    
    return cell;
}
@end
