//
//  LXRSettingViewController.m
//  01- 百思不得姐
//
//  Created by mac on 16/7/24.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LXRSettingViewController.h"

#import "SDImageCache.h"
@interface LXRSettingViewController ()

@end

@implementation LXRSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"设置";
    self.tableView.backgroundColor = LXRTagBg;
    
    
    
    
}
#pragma mark 第一种方法->遍历所有文件大小
-(void)getSize{

    //创建文件管理器
    NSFileManager* manager = [NSFileManager defaultManager];
    //获取NSCachesDirectory文件路径
    NSString* caches = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)lastObject];
    //拼接图片缓存文件夹
    NSString* cachesPath = [caches stringByAppendingPathComponent:@"com.hackemist.SDWebImageCache.default"];
    //创建文件遍历 对象(用文件管理对象->根据图片缓存路径)
    NSDirectoryEnumerator* fileEnumerator = [manager enumeratorAtPath:cachesPath];
    //定义文件总大小变量
    NSInteger totalSize = 0;
    for (NSString* fileName in fileEnumerator) {
        //拼接图片文件路径
        NSString* filePath = [cachesPath stringByAppendingPathComponent:fileName];
        //用文件管理对象根据图片文件路径,得到没有图片文件详情
        NSDictionary* attrs = [manager attributesOfItemAtPath:filePath error:nil];
        //如果文件类型是文件夹类型->返回
        if ([attrs[NSFileType] isEqualToString:NSFileTypeDirectory]) continue;
        //统计所有文件总大小
        totalSize += [attrs[NSFileSize] integerValue];
    }
}
#pragma mark 第二种方法->遍历所有文件大小 推荐
-(void)getSize2{

    //创建文件管理器
    NSFileManager* manager = [NSFileManager defaultManager];
    //获取NSCachesDirectory文件路径
    NSString* caches = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)lastObject];
    //拼接图片缓存文件夹
    NSString* cachesPath = [caches stringByAppendingPathComponent:@"com.hackemist.SDWebImageCache.default"];
    
    //获得文件夹内部的所有内容
    //1.获得当前文件夹的所有内容,局限一层
    //NSArray* contents = [manager contentsOfDirectoryAtPath:cachesPath error:nil];
    //2.获得当前文件夹的所有内容,所有层  推荐!!!
    NSArray* subPaths = [manager subpathsAtPath:cachesPath];
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString* ID = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    //图片缓存
    CGFloat size = [SDImageCache sharedImageCache].getSize / 1000.0 / 1000;
    cell.textLabel.text = [NSString stringWithFormat:@"清除缓存(已使用%.1fMB)",size];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [[SDImageCache sharedImageCache]cleanDiskWithCompletionBlock:^{
        //清除成功
    }];
    //第二种方法,系统自带
    //[NSFileManager defaultManager]removeItemAtPath:<#(nonnull NSString *)#> error:<#(NSError * _Nullable __autoreleasing * _Nullable)#>
}


@end
