//
//  LXRWebViewController.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/30.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LXRWebViewController.h"
#import "NJKWebViewProgress.h"


@interface LXRWebViewController ()<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *goBackItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *goForwardItem;
/**进度代理对象*/
@property(nonatomic,strong)NJKWebViewProgress* progress;
/**进度条控件*/
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;

@end

@implementation LXRWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //进度代理对象
    self.progress = [[NJKWebViewProgress alloc]init];
    
    self.webView.delegate = self.progress;
    
    __weak typeof (self) weakSelf = self;
    //进度值
    self.progress.progressBlock = ^(float progress){
        LXRLog(@"%f",progress);
        weakSelf.progressView.progress = progress;
        weakSelf.progressView.hidden = (progress == 1.0);
    };
    
    //让控制器代理可以监听webView
    self.progress.webViewProxyDelegate = self;
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
    
}
- (IBAction)goBack:(id)sender {
    [self.webView goBack];
}
- (IBAction)goForward:(id)sender {
    [self.webView goForward];
}
- (IBAction)refresh:(id)sender {
    [self.webView reload];
}
#pragma mark - <UIWebViewDelegate>
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    self.goBackItem.enabled = webView.canGoBack;
    self.goForwardItem.enabled = webView.canGoForward;
}


@end
