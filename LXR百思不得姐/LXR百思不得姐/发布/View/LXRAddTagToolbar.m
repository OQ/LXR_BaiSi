//
//  LXRAddTagToolbar.m
//  01- 百思不得姐
//
//  Created by mac on 16/7/2.
//  Copyright © 2016年 mac. All rights reserved.
//

/*
如果控制器'a'->通过Push->另一个控制器'b',控制器'b'要拿到'a'导航栏控制器
 1.取出当前的TabBarController->通过系统keyWindow的跟控制器拿到
UITabBarController *tabBarVc = (UITabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
 2.在通过拿到的tabBarVc->通过当前选中的selectedViewController拿到当前所在的导航控制器
UINavigationController *navVc = (UINavigationController *)tabBarVc.selectedViewController;
 3.通过Push到下一界面
[navVc pushViewController:想要跳转到的控制器 animated:YES];
 
 
如果控制器'a'->通过modal->另一个控制器'b',控制器'b'要拿到'a'导航栏控制器
根据展示控制器的属性
a.presentedViewController -> b控制器
b.presentingViewController -> a控制器
UIViewController* root = [UIApplication sharedApplication].keyWindow.rootViewController;
UINavigationController* navVc = (UINavigationController*)root.presentedViewController;
[navVc pushViewController:想要跳转到的控制器 animated:YES];
*/

#import "LXRAddTagToolbar.h"
#import "LXRAddTagViewController.h"
@interface LXRAddTagToolbar ()
/** 顶部控件 */
@property (weak, nonatomic) IBOutlet UIView *topView;
/** 添加按钮 */
@property (weak, nonatomic) UIButton *addButton;
/** 存放所有的Lable */
@property (strong,nonatomic) NSMutableArray* tagLables;
@end

@implementation LXRAddTagToolbar

-(NSMutableArray *)tagLables{
    if (_tagLables == nil) {
        _tagLables = [NSMutableArray new];
    }
    return _tagLables;
}

-(void)awakeFromNib{
    //默认拥有两个标签
    [self createTagLables:@[@"吐槽",@"糗事"]];
    
    //添加一个加号按钮
    UIButton* addButton = [[UIButton alloc]init];
    [addButton setImage:[UIImage imageNamed:@"tag_add_icon"] forState:UIControlStateNormal];
    /**得到按钮有图片时的Size,和图片大小一样*/
    //addButton.size = [UIImage imageNamed:@"tag_add_icon"].size;
    //addButton.size = [addButton imageForState:UIControlStateNormal].size;
    addButton.size = addButton.currentImage.size;
    addButton.x = LXRTagMargin;
    //点击
    [addButton addTarget:self action:@selector(addButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:addButton];
    self.addButton = addButton;
    
    
}

-(void)layoutSubviews{
    [super layoutSubviews];
    for (int i = 0; i<self.tagLables.count; i++) {
        UILabel *tagLabel = self.tagLables[i];
        
        // 设置位置
        if (i == 0) { // 最前面的标签
            tagLabel.x = 0;
            tagLabel.y = 0;
        } else { // 其他标签
            UILabel *lastTagLabel = self.tagLables[i - 1];
            // 计算当前行左边的宽度
            CGFloat leftWidth = CGRectGetMaxX(lastTagLabel.frame) + LXRTagMargin;
            // 计算当前行右边的宽度
            CGFloat rightWidth = self.topView.width - leftWidth;
            if (rightWidth >= tagLabel.width) { // 按钮显示在当前行
                tagLabel.y = lastTagLabel.y;
                tagLabel.x = leftWidth;
            } else { // 按钮显示在下一行
                tagLabel.x = 0;
                tagLabel.y = CGRectGetMaxY(lastTagLabel.frame) + LXRTagMargin;
            }
        }
    }
    
    // 最后一个标签
    UILabel *lastTagLabel = [self.tagLables lastObject];
    CGFloat leftWidth = CGRectGetMaxX(lastTagLabel.frame) + LXRTagMargin;
    
    // 更新textField的frame
    if (self.topView.width - leftWidth >= self.addButton.width) {
        self.addButton.y = lastTagLabel.y;
        self.addButton.x = leftWidth;
    } else {
        self.addButton.x = 0;
        self.addButton.y = CGRectGetMaxY(lastTagLabel.frame) + LXRTagMargin;
    }
    
    // 整体的高度
    CGFloat oldH = self.height;
    self.height = CGRectGetMaxY(self.addButton.frame) + 45;
    self.y -= self.height - oldH;
}


/** 创建标签 */
-(void)createTagLables:(NSArray*)tags{
    //清空之前所有Lable控件
    [self.tagLables makeObjectsPerformSelector:@selector(removeFromSuperview)];
    //清空数组
    [self.tagLables removeAllObjects];
    
    for (int i = 0; i<tags.count; i++) {
        UILabel *tagLabel = [[UILabel alloc] init];
        [self.tagLables addObject:tagLabel];
        tagLabel.backgroundColor = LXRTagBg;
        tagLabel.textAlignment = NSTextAlignmentCenter;
        tagLabel.text = tags[i];
        tagLabel.font = LXRTagFont;
        // 应该要先设置文字和字体后，再进行计算
        [tagLabel sizeToFit];
        tagLabel.width += 2 * LXRTagMargin;
        tagLabel.height = LXRTagH;
        tagLabel.textColor = [UIColor whiteColor];
        [self.topView addSubview:tagLabel];
    }

    // 重新布局子控件
    [self setNeedsLayout];
}

/** 点击加号按钮 */
-(void)addButtonClick{
    //目标控制器
    LXRAddTagViewController* TagVc = [[LXRAddTagViewController alloc]init];
    //防止循环引用
    __weak typeof(self) weakSelf = self;
    [TagVc setTagsBlock:^(NSArray *tags) {
        [weakSelf createTagLables:tags];
    }];
    TagVc.tags = [self.tagLables valueForKeyPath:@"text"];
    
    //取出跟控制器
    UIViewController* root = [UIApplication sharedApplication].keyWindow.rootViewController;
    //根据根控制器的presentViewController,取出导航栏控制器
    UINavigationController*  navVc = (UINavigationController*)root.presentedViewController;
    //取出的导航栏控制器,进行Push到下一界面
    [navVc pushViewController:TagVc animated:YES];
}
@end
