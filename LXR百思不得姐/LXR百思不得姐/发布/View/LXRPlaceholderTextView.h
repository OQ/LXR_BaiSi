//
//  LXRPlaceholderTextView.h
//  01- 百思不得姐
//
//  Created by mac on 16/7/1.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LXRPlaceholderTextView : UITextView
/** 占位文字 */
@property(nonatomic,copy)NSString* placeholder;         //默认大小15
/** 占位文字颜色 */
@property(nonatomic,strong)UIColor* placeholderColer;   //默认颜色灰色
@end
