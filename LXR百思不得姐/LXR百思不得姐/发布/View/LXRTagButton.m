//
//  LXRTagButton.m
//  01- 百思不得姐
//
//  Created by MACBOOK on 16/7/16.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LXRTagButton.h"

@implementation LXRTagButton

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setImage:[UIImage imageNamed:@"chose_tag_close_icon"] forState:UIControlStateNormal];
        self.titleLabel.font = LXRTagFont;
        [self setBackgroundColor:LXRTagBg];
        
    }
    return self;
}

-(void)setTitle:(NSString *)title forState:(UIControlState)state{
    [super setTitle:title forState:state];
    
    [self sizeToFit];
    
    self.width = self.width + 3 * LXRTagMargin;
    self.height = LXRTagH;
}

/** 重新布局按钮内部控件位置*/
-(void)layoutSubviews{
    [super layoutSubviews];
    
    self.titleLabel.x = LXRTagMargin;
    
    self.imageView.x = CGRectGetMaxX(self.titleLabel.frame) + LXRTagMargin;
}



@end
