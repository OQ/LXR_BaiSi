//
//  LXRAddTagViewController.h
//  01- 百思不得姐
//
//  Created by mac on 16/7/2.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LXRAddTagViewController : UIViewController
/** 传递给上一个控制器的Block*/
@property(nonatomic,copy)void(^tagsBlock)(NSArray* tags);
/** 所有标签*/
@property(nonatomic,strong)NSArray* tags;
@end
