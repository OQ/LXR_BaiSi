



#import <UIKit/UIKit.h>
/**tabBar被选中的通知名字*/
NSString* const LXRTabBarDidSelectNotification = @"LXRTabBarDidSelectNotification";
/**tabBar被选中的通知->被选中的控制器的Index key*/
NSString* const LXRSelectedControllerIndexKey = @"LXRSelectedControllerIndexKey";
/**tabBar被选中的通知->被选中的控制器的key*/
NSString* const LXRSelectedControllerKey = @"LXRSelectedControllerKey";

/**************************************************************/

/**标签-间距*/
CGFloat const LXRTagMargin = 5;
/**标签-高度*/
CGFloat const LXRTagH = 25;

/**************************************************************/

/**精华-顶部标题的高度*/
CGFloat const LXRTitlesViewH = 35;
/**精华-顶部标题的Y*/
CGFloat const LXRTitlesViewY = 64;
/**精华-cell-间距*/
CGFloat const LXRTopicCellMargin = 10;
/**精华-cell-文字内容的Y值*/
CGFloat const LXRTopicCellTextY = 55;
/**精华-cell-顶部工具条的高度*/
CGFloat const LXRTopicCellBottomBarH = 44;
/**精华-cell-图片帖子的最大高度*/
CGFloat const LXRTopicCellPictureMaxH = 1000;
/**精华-cell-图片帖子一旦超过最大高度,就用Break高度*/
CGFloat const LXRTopicCellPictureBreakH = 250;
/** 精华-cell-最热评论标题的高度 */
CGFloat const LXRTopicCellTopCmtTitleH = 20;
/**LXRUserModel模型-->性别属性值*/
NSString* const LXRUserSexMale = @"m";
NSString* const LXRUserSexFemale = @"f";



