//
//  UIImage+LXRExtension.h
//  01- 百思不得姐
//
//  Created by mac on 16/6/30.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (LXRExtension)
/**
 *  圆形图片
 */
-(UIImage *)circleImage;
@end
