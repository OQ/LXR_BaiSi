//
//  NSDictionary+Extension.m
//  01- 掌握-JSON解析
//
//  Created by mac on 16/5/25.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@implementation NSDictionary (Log)
-(NSString *)descriptionWithLocale:(id)locale{
    //创建可变字符串 进行以下拼接
    NSMutableString* string = [NSMutableString string];
    //开头有个{
    [string appendString:@"{\n"];
    //遍历所有键值对
    [self enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        //key
        [string appendFormat:@"\t%@",key];
        [string appendString:@" : "];
        //value
        //[string appendString:[obj description]];
        [string appendFormat:@"%@,\n",obj];
    }];
    //结尾有个}
    [string appendString:@"}"];
    
    //删除最后一个逗号  NSBackwardsSearch从后往前找
    NSRange last = [string rangeOfString:@"," options:NSBackwardsSearch];
    [string deleteCharactersInRange:last];

    return string;
}
@end



@implementation NSArray (Log)

-(NSString *)descriptionWithLocale:(id)locale{
    //创建可变字符串 进行以下拼接
    NSMutableString* string = [NSMutableString string];
    //开头有个[
    [string appendString:@"[\n"];
    //遍历所有元素
    [self enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        [string appendFormat:@"%@,\n",obj];
        
    }];
    
    //结尾有个]
    [string appendString:@"]"];
    
    //删除最后一个逗号
    NSRange last = [string rangeOfString:@"," options:NSBackwardsSearch];
    //如果数组里没有元素需要加判断，否则报错
    if (last.location != NSNotFound)
    [string deleteCharactersInRange:last];
    return string;
}

@end