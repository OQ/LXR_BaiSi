//
//  UIBarButtonItem+LXRExtension.h
//  01- 百思不得姐
//
//  Created by mac on 16/6/7.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (LXRExtension)
/**
 *  创建BarButtonItem
 *
 *  @param image     普通状态图片
 *  @param highImage 高亮状态图片
 *  @param action    点击方法
 */
+(instancetype)itemWithImage:(NSString*)image highImage:(NSString*)highImage target:(id)target action:(SEL)action;


@end
