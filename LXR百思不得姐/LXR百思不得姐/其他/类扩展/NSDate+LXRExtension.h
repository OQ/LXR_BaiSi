//
//  NSDate+LXRExtension.h
//  01- 百思不得姐
//
//  Created by mac on 16/6/22.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (LXRExtension)
/**
*  比较from和self时间差值
*/
-(NSDateComponents*)deltaFrom:(NSDate*)from;

/**
 *是否是今年
 */
-(BOOL)isThisYear;
/**
 *是否是今天
 */
-(BOOL)isToday;
/**
 *是否是昨天
 */
-(BOOL)isYesterday;

@end
