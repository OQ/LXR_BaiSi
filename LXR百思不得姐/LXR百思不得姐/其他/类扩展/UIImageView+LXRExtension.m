//
//  UIImageView+LXRExtension.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/30.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "UIImageView+LXRExtension.h"
#import "UIImageView+WebCache.h"


@implementation UIImageView (LXRExtension)
/** 设置圆形头像 */
-(void)setCircleHeader:(NSString *)url
{
    //占位图片
    UIImage* placeImage = [[UIImage imageNamed:@"defaultUserIcon"] circleImage];
    //SDWebImage下载图片
    [self sd_setImageWithURL:[NSURL URLWithString:url]placeholderImage:placeImage
        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            //图像设置圆形
            self.image = image ? [image circleImage] : placeImage;
        }];
    
}
@end
