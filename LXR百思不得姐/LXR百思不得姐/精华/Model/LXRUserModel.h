//
//  LXRUserModel.h
//  01- 百思不得姐
//
//  Created by mac on 16/6/27.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LXRUserModel : NSObject
/**用户名*/
@property(nonatomic,copy)NSString* username;
/**性别*/
@property(nonatomic,copy)NSString* sex;
/**头像*/
@property(nonatomic,copy)NSString* profile_image;
@end
