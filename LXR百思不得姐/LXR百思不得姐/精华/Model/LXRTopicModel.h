//
//  LXRTopicModel.h
//  01- 百思不得姐
//
//  Created by mac on 16/6/17.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LXRTopicModel : NSObject

/*****************控件基本属性*****************/
/**id*/
@property(nonatomic,copy)NSString* ID;
/**帖子的类型*/
@property(nonatomic,assign)LXRTopicType type;
/**名称*/
@property(nonatomic,copy)NSString* name;
/**头像照片*/
@property(nonatomic,copy)NSString* profile_image;
/**文字内容*/
@property(nonatomic,copy)NSString* text;
/**发帖时间*/
@property(nonatomic,copy)NSString* create_time;
/**顶的数量*/
@property(nonatomic)NSInteger ding;
/**cai的数量*/
@property(nonatomic)NSInteger cai;
/**转发的数量*/
@property(nonatomic)NSInteger repost;
/**评论的数量*/
@property(nonatomic)NSInteger comment;
/**新浪加V*/
@property(nonatomic)BOOL sina_v;
/**最热评论(期望这个数组中存放的是LXRComment模型)*/
@property(nonatomic,strong)NSArray* top_cmt;

/*****************图片帖子属性*****************/
/**图片的宽度*/
@property(nonatomic,assign)CGFloat width;
/**图片的高度*/
@property(nonatomic,assign)CGFloat height;
/**小图片的URL*/
@property(nonatomic,copy)NSString* small_image;
/**中图片的URL*/
@property(nonatomic,copy)NSString* middle_image;
/**大图片的URL*/
@property(nonatomic,copy)NSString* large_image;

/*****************声音/视频帖子属性*****************/
/**播放次数*/
@property(nonatomic,assign)NSInteger playcount;
/**音频时间  返回的毫秒*/
@property(nonatomic,assign)NSInteger voicetime;
/**音频的播放地址*/
@property(nonatomic,copy)NSString* voiceuri;


/**视频时间  返回的毫秒*/
@property(nonatomic,assign)NSInteger videotime;


/*****************额外的辅助属性*****************/
/**
 *cell的高度
 *当修饰符设置readonly,要在.m中在创建一个_cellHeight,不然报错
 */
@property(nonatomic,readonly)CGFloat cellHeight;
/** 图片控件的Frame*/
@property(nonatomic,assign,readonly)CGRect pictureF;
/**图片是否太大*/
@property(nonatomic,assign,getter=isBigPicture)BOOL bigPicture;
/**图片的下载进度*/
@property(nonatomic,assign)CGFloat pictureProgress;

/** 声音控件的Frame*/
@property(nonatomic,assign,readonly)CGRect voiceF;
/** 视频控件的Frame*/
@property(nonatomic,assign,readonly)CGRect videoF;
@end
