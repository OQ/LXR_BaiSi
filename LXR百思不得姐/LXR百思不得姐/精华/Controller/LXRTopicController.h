//
//  LXRTopicController.h
//  01- 百思不得姐
//
//  Created by mac on 16/6/22.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LXRTopicController : UITableViewController
/** 帖子类型(交给子类去实现) */
@property(nonatomic,assign)LXRTopicType type;

@end
