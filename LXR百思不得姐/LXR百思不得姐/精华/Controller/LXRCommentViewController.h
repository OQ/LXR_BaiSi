//
//  LXRCommentViewController.h
//  01- 百思不得姐
//
//  Created by mac on 16/6/28.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LXRTopicModel;
@interface LXRCommentViewController : UIViewController
/**帖子模型*/
@property(nonatomic,strong)LXRTopicModel* model;
@end
