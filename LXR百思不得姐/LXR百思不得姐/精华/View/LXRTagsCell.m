//
//  LXRTagsCell.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/12.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LXRTagsCell.h"
#import "LXRTagsModel.h"
#import "UIImageView+WebCache.h"

@interface LXRTagsCell ()
@property (weak, nonatomic) IBOutlet UIImageView *image_list_Image;
@property (weak, nonatomic) IBOutlet UILabel *themeNameLable;
@property (weak, nonatomic) IBOutlet UILabel *subNumberLable;

@end



@implementation LXRTagsCell


-(void)setTagModel:(LXRTagsModel *)tagModel{

    _tagModel = tagModel;
    
    //设置圆形头像
    [self.image_list_Image setCircleHeader:tagModel.image_list];
    
    self.themeNameLable.text = tagModel.theme_name;
    
    NSString *subNumber = nil;
    if (tagModel.sub_number < 10000) { //订阅人数少于10000,正常显示
        subNumber = [NSString stringWithFormat:@"%zd人订阅",tagModel.sub_number];
    }else{  //订阅数大于10000,处理后显示
        subNumber = [NSString stringWithFormat:@"%.1f万人订阅",tagModel.sub_number/10000.0];
    }
    self.subNumberLable.text = subNumber;
}

/**
 *  需要重写setFrame和setBounds方法作用:
 *  重新布局cell,拦截设置方法后进行重新赋值,别人无法改变
 */
-(void)setFrame:(CGRect)frame{
    //cell效果,x往右移动10,宽度减少2倍的x,高度减少1
    //frame.origin.x = 10;
    //frame.size.width -= 2*frame.origin.x;
    frame.size.height -= 1;
    
    [super setFrame:frame];
    
}

-(void)setBounds:(CGRect)bounds{
    //cell效果,x往右移动10,宽度减少2倍的x,高度减少1
    bounds.origin.x = 10;
    bounds.size.width -= 2*bounds.origin.x;
    bounds.size.height -= 1;
    [super setBounds:bounds];
}

@end
