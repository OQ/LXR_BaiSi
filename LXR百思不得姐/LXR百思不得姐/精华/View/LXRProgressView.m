//
//  LXRProgressView.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/24.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "LXRProgressView.h"

@implementation LXRProgressView
-(void)awakeFromNib{
    //设置进度条圆角
    self.roundedCorners = 2;
    //设置进度条文字颜色
    self.progressLabel.textColor = [UIColor whiteColor];
}
-(void)setProgress:(CGFloat)progress animated:(BOOL)animated{
    [super setProgress:progress animated:animated];
    //解决进度条显示负数问题
    NSString* text = [NSString stringWithFormat:@"%.0f%%",progress*100];
    //把用负号字符串替换为空字符串
    text = [text stringByReplacingOccurrencesOfString:@"-" withString:@""];
    //进度条中间Lable文字 百分比 显示
    self.progressLabel.text = text;
}
@end
