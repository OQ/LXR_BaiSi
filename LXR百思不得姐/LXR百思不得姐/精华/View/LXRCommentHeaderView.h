//
//  LXRCommentHeaderView.h
//  01- 百思不得姐
//
//  Created by mac on 16/6/28.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LXRCommentHeaderView : UITableViewHeaderFooterView
/**标题*/
@property(nonatomic,copy)NSString* title;
/**根据TableView创建头部视图*/
+(instancetype)headerViewWithTableView:(UITableView*)tableView;

@end
