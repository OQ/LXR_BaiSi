//
//  LXRTagsCell.h
//  01- 百思不得姐
//
//  Created by mac on 16/6/12.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LXRTagsModel;
@interface LXRTagsCell : UITableViewCell
/**模型*/
@property(nonatomic,strong)LXRTagsModel* tagModel;
@end
