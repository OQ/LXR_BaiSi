//
//  LXRCommectCell.h
//  01- 百思不得姐
//
//  Created by mac on 16/6/29.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LXRCommentModel;
@interface LXRCommectCell : UITableViewCell

/**评论模型*/
@property(nonatomic,strong)LXRCommentModel* model;

@end
