//
//  LXRTopicPictureView.m
//  01- 百思不得姐
//
//  Created by mac on 16/6/23.
//  Copyright © 2016年 mac. All rights reserved.

//  图片中间的内容

#import "LXRTopicPictureView.h"
#import "LXRTopicModel.h"
#import "LXRShowPictureViewController.h"

#import "UIImageView+WebCache.h"
/**重新包装第三方进度条类,防止以后不用这个框架所要修改的内容*/
#import "LXRProgressView.h"
@interface LXRTopicPictureView ()
/** 图片 */
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
/** gif标识 */
@property (weak, nonatomic) IBOutlet UIImageView *gifView;
/** 查看大图按钮 */
@property (weak, nonatomic) IBOutlet UIButton *seeBigBtn;
/** 进度条控件 */
@property (weak, nonatomic) IBOutlet LXRProgressView *ProgressView;

@end

@implementation LXRTopicPictureView


-(void)awakeFromNib{
    //当设置好Frame,打印结果与设置无问题的时候,达不到预期的效果
    //首先考虑是系统的自动调整属性-->设置不用自动调整
    self.autoresizingMask = UIViewAutoresizingNone;
    
    //给图片添加监听器
    self.imageView.userInteractionEnabled = YES;
    [self.imageView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showPicture)]];
    
    

}
/** 当点击图片时*/
//点击查看按钮时每反应,解决方法让点击查看按钮不能点击
-(void)showPicture{
    LXRShowPictureViewController* showPicture = [[LXRShowPictureViewController alloc]init];
    showPicture.model = self.model;
    //模态跳转  让当前不是控制器时,调用窗口根控制器来执行模态跳转
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:showPicture animated:YES completion:nil];
}



-(void)setModel:(LXRTopicModel *)model{
    _model = model;
    
//立马显示最新的进度值(防止因为网速慢,导致显示的是其他图片的下载进度)
    [self.ProgressView setProgress:model.pictureProgress animated:NO];
    
//下载图片
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:model.large_image] placeholderImage:nil options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
        //显示进度条
        self.ProgressView.hidden = NO;
        
        //计算图片下载进度值
         model.pictureProgress = 1.0*receivedSize/expectedSize;
      
        //显示进度值
        [self.ProgressView setProgress:model.pictureProgress animated:NO];
       
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        //隐藏进度条
        self.ProgressView.hidden = YES;
        
        //如果是大图片,才需要进行绘图处理
        if (model.isBigPicture == NO) return ;
        
        //用2D重新画图片
        //开启图片上下文
        UIGraphicsBeginImageContextWithOptions(model.pictureF.size, YES, 0.0);
        //将下载完的image对象绘制到图形上下文
        CGFloat width = model.pictureF.size.width;
        CGFloat height = width*image.size.height/image.size.width;//交叉相乘得到等比例的高度
        [image drawInRect:CGRectMake(0, 0, width, height)];
        //获得图片
        self.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
        //结束图像上下文
        UIGraphicsEndImageContext();
    }];
    
//判断是否是Gif
    //SDImage框架处理Gif图片---->苹果自带ImageIO框架-->Gif-->N个UIImage
    NSString* extension = [model.large_image pathExtension];//取出资源的后缀
    //先把后缀名转化为小写字母,然后进行判断是不是gif格式,如果不是就隐藏
    self.gifView.hidden = ![extension.lowercaseString isEqualToString:@"gif"];
    
//判断是否显示"点击查看按钮"
    if (model.isBigPicture) {//大图
        self.seeBigBtn.hidden = NO;
        //self.imageView.contentMode = UIViewContentModeScaleAspectFill;//|UIViewContentModeTop;
    }else{ //非大图
        self.seeBigBtn.hidden = YES;
        //self.imageView.contentMode = UIViewContentModeScaleToFill;
    }
}
@end
